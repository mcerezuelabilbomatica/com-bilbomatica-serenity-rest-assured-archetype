package com.bilbomatica.serenity.bdd.serenitySteps;



import io.restassured.response.Response;
import org.junit.Assert;

import static net.serenitybdd.rest.SerenityRest.given;
import static net.serenitybdd.rest.SerenityRest.rest;

public class ExampleSteps {

    private String baseUri;
    private Response response;

    public void sendGetRequest(String arg0) {
        this.setBaseUri(arg0);
        System.out.println("\nGET SERVICE URL: "+baseUri+"\n");
       response = rest().get(baseUri).then().extract().response();
        //given().when().get(baseUri).then().statusCode(200);

    }

    private void setBaseUri(String uri) {
        baseUri = uri;
    }

    public void verifyResponseCode(int arg1) {
        System.out.println("\nGET RESPONSE CODE URL: "+response.getStatusCode());
        Assert.assertEquals("Status code must be: ",arg1,response.getStatusCode());

    }
}
