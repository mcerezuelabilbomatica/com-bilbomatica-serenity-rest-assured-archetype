package com.bilbomatica.serenity.bdd.gherkinDefinitions;

import com.bilbomatica.serenity.bdd.serenitySteps.ExampleSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class ExampleDefinitions {
    @Steps
    ExampleSteps exampleSteps = new ExampleSteps();

    @Given("^Connection Service GET listens on \"([^\"]*)\"$")
    public void connectionServiceGetListensOn(String arg0){
       exampleSteps.sendGetRequest(arg0);
    }
    @Then("^I should get a (\\d+) response code from Connection Service$")
    public void i_should_get_a_response_code_from_Connection_Service(int arg1) throws Exception {
        exampleSteps.verifyResponseCode(arg1);
    }
}
